using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using NLog.Web;

namespace AspCore
{
    public class Program
    {
        public static string sourceSwitchName { get; private set; }

        public static void Main(string[] args)
        {
            // NLog: setup the logger first to catch all errors
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug("init main");
                CreateWebHostBuilder(args).Build().Run();

            }
            catch (Exception ex)
            {
                //NLog: catch setup errors
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }
        //static void Main(string[] args)
        //{
        //    ILoggerFactory loggerFactory = new LoggerFactory().AddConsole((_, __) => true);
        //    ILogger logger = loggerFactory.CreateLogger<Program>();

        //    logger.LogInformation("Logging information.");
        //    logger.LogCritical("Logging critical information.");
        //    logger.LogDebug("Logging debug information.");
        //    logger.LogError("Logging error information.");
        //    logger.LogTrace("Logging trace");
        //    logger.LogWarning("Logging warning.");
        //    BuildWebHost(args).Run();
        //}
        //public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //        .UseStartup<Startup>();

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
WebHost.CreateDefaultBuilder(args)
    .UseStartup<Startup>()
            .UseIISIntegration()
            .UseKestrel()
            .UseContentRoot(Directory.GetCurrentDirectory())

    .ConfigureLogging(logging =>
    {
        logging.ClearProviders();
        logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
    })
    .UseNLog();  // NLog: setup NLog for Dependency injection



        //public static IWebHost BuildWebHost(string[] args) =>
        //WebHost.CreateDefaultBuilder(args)
        //     //.ConfigureLogging(logBuilder =>
        //     //{
        //     //    logBuilder.ClearProviders(); // removes all providers from LoggerFactory
        //     //    logBuilder.AddConsole();
        //     //    logBuilder.AddDebug();
        //     //    logBuilder.AddEventLog();
        //     //    logBuilder.AddEventSourceLogger();
        //     //    logBuilder.AddTraceSource("Information, ActivityTracing"); // Add Trace listener provider
        //     //})

        //    .UseStartup<Startup>()
        //    .UseUrls("http://localhost:44354/")
        //    .Build();



    }
}
