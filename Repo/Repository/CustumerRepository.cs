﻿using Repo.Models.Context;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Repo.Models.CustumModels;

namespace Repo.Repository
{
   public class CustumerRepository : ICustumer
    {


        private SampleDbContext _context;

        public CustumerRepository(SampleDbContext context)
        {
            _context = context;
        }
       
            public  string GetHTMLString()
            {
                var employees = _context.custumerDetails.ToList();

                var sb = new StringBuilder();
                sb.Append(@"
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='header'><h1>This is the generated PDF report!!!</h1></div>
                                <table align='center'>
                                    <tr>
                                        <th>Contact Name</th>
                                        <th>Contact Title</th>
                                        <th>Address</th>
                                        <th>City</th>
<th>Region</th>
<th>Country</th>
<th>Phone</th>
                                    </tr>");

                foreach (var emp in employees)
                {
                    sb.AppendFormat(@"<tr>
                                    <td>{0}</td>
                                    <td>{1}</td>
                                    <td>{2}</td>
                                    <td>{3}</td>
<td>{4}</td>
                                    <td>{5}</td>
<td>{6}</td>
                                  </tr>", emp.ContactName, emp.ContactTitle, emp.Address, emp.City,emp.Region,emp.Country,emp.Phone);
                }

                sb.Append(@"
                                </table>
                            </body>
                        </html>");

                return sb.ToString();
            }
        
        public CumCustumer SortCustumerDetails(string data, string sort)
        {
            List<Custumer> cs = new List<Custumer>();
            CumCustumer cc = new CumCustumer();
            var total = _context.custumerDetails.Count();
            var pageSize = 5;

            var page = 1;

            var skip = pageSize * (page - 1);

            var canPage = skip < total;

            if (!canPage)
                return cc;
            if (sort=="Dec")
            {
                if (data == "name")
                {
                    cs = _context.custumerDetails.OrderByDescending(s => s.ContactName).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "contactTitle")
                {
                    cs = _context.custumerDetails.OrderByDescending(s => s.ContactTitle).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "Address")
                {
                    cs = _context.custumerDetails.OrderByDescending(s => s.Address).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "City")
                {
                    cs = _context.custumerDetails.OrderByDescending(s => s.City).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "Region")
                {
                    cs = _context.custumerDetails.OrderByDescending(s => s.Region).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "Country")
                {
                    cs = _context.custumerDetails.OrderByDescending(s => s.Country).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "Phone")
                {
                    cs = _context.custumerDetails.OrderByDescending(s => s.Phone).Skip(skip).Take(pageSize).ToList();

                }
            }
            if (sort == "Asc")
            {
                if (data == "name")
                {
                    cs = _context.custumerDetails.OrderBy(s => s.ContactName).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "contactTitle")
                {
                    cs = _context.custumerDetails.OrderBy(s => s.ContactTitle).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "Address")
                {
                    cs = _context.custumerDetails.OrderBy(s => s.Address).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "City")
                {
                    cs = _context.custumerDetails.OrderBy(s => s.City).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "Region")
                {
                    cs = _context.custumerDetails.OrderBy(s => s.Region).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "Country")
                {
                    cs = _context.custumerDetails.OrderBy(s => s.Country).Skip(skip).Take(pageSize).ToList();

                }
                if (data == "Phone")
                {
                    cs = _context.custumerDetails.OrderBy(s => s.Phone).Skip(skip).Take(pageSize).ToList();

                }

            }
            cc.cusDetails = cs;
            cc.totalcount = total;
            cc.count = cs.Count;
            return cc;
        }
        public CumCustumer getCustumersRepo(searchpage request)
        {
            List<Custumer> cs = new List<Custumer>();
            CumCustumer cc = new CumCustumer();
            var total = _context.custumerDetails.Count();
            var pageSize = request.pageSize; 

            var page = request.pageIndex; 

            var skip = pageSize * (page - 1);

            var canPage = skip < total;

            if (!canPage)
                return cc;
            if (request.searchdata != null && request.searchdata != "")
            {
                cs = _context.custumerDetails.Where(s => s.Address.Contains(request.searchdata) || s.ContactName.Contains(request.searchdata) || s.ContactTitle.Contains(request.searchdata) || s.Address.Contains(request.searchdata) || s.City.Contains(request.searchdata) || s.Region.Contains(request.searchdata) || s.Country.Contains(request.searchdata) || s.Phone.Contains(request.searchdata)).OrderBy(s => s.Id).Skip(skip).Take(pageSize).ToList();
                //cs = _context.custumerDetails.ToList();
            }
            else
            {
                cs = _context.custumerDetails.OrderBy(s => s.Id).Skip(skip).Take(pageSize).ToList();

            }
            cc.cusDetails = cs;
            cc.totalcount = total;
            cc.count = cs.Count;
            return cc;
        }
      public  string saveCustumerDetails(Custumer request)
        {
            Custumer cs = new Custumer();
            try
            {
                cs.ContactName = request.ContactName;
                cs.ContactTitle = request.ContactTitle;
                cs.Address = request.Address;
                cs.City = request.City;
                cs.Country = request.Country;
                cs.Phone = request.Phone;
                cs.Region = request.Region;
                _context.custumerDetails.Add(cs);
                _context.SaveChanges();
                var Response = "true";
                return Response;
            }
            catch(Exception e)
            {
                return e.InnerException.ToString();
            }
            
        }

        public Custumer getCustumerDetailsById(int Id)
        {
            Custumer cs = new Custumer();
            cs = _context.custumerDetails.Where(s=> s.Id== Id).FirstOrDefault();
            return cs;
        }
        public string updateCustumerDetails(Custumer request)
        {
            
            Custumer cs = new Custumer();
            try
            {
                Custumer userData = _context.custumerDetails.Where(u => u.Id == request.Id).SingleOrDefault();
               
                userData.Id = request.Id;
                userData.ContactName = request.ContactName;
                userData.ContactTitle = request.ContactTitle;
                userData.Address = request.Address;
                userData.City = request.City;
                userData.Country = request.Country;
                userData.Phone = request.Phone;
                userData.Region = request.Region;
               
                _context.SaveChanges();
                var Response = "true";
                return Response;
            }
            catch (Exception e)
            {
                return e.InnerException.ToString();
            }
        }
        public string deleteCustumerDetailsById(int Id)
        {
            Custumer c = new Custumer();
            try
            {
                Custumer user = _context.custumerDetails.Where(u => u.Id == Id).SingleOrDefault();
                _context.custumerDetails.Remove(user);
                _context.SaveChanges();
                var Response = "true";
                return Response; ;

            }
            catch (Exception e)
            {
                return e.InnerException.ToString();
            }
        }

        public List<Custumer> ExportData()
        {
            List<Custumer> c = new List<Custumer>();
            try
            {
                c = _context.custumerDetails.ToList();
                return c;
            }
            catch (Exception e)
            {
                var s = e;
                return c;
            }
        }
    }
}
